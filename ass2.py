#
# file: ass2.py
# author: 93059239 Thomas Ekholm
# purpose: 159.251 Assignment 2
#
# to do: 
# test on raspberry pi
# convert to spaces not tabs
# rename to mybackup.py
#
#
# note on prefix notation:
#  l_ = list
#  h_ = hash/dictionary
# all routines, when possible, declared before actual usage
#
import sys,os,os.path
import pickle  # combine?
import inspect
import hashlib, time
import shutil


#
# globals
#
#myArchiveDirPath=os.path.expanduser("~/myArchive") # forward slash safe on all operating systems, ~=home directory
myArchiveDirPath=os.path.expanduser("~"+os.sep+"myArchive") 
#
indexFilename="index.txt"  
indexFilePath=os.path.join(myArchiveDirPath,indexFilename)
#
objectsDir="objects"
objectsDirPath=os.path.join(myArchiveDirPath,objectsDir)
#
logFilename="myBackupLog.txt" 
logFilePath=os.path.join(myArchiveDirPath,logFilename)
#
DELIM = "," # define the delimiter used in the logging
MAX_MATCHES=3 # define the maximum number of matches that will be displayed in routine get

def log(tag,message):
	#
	ts = time.time()
	#print(ts)
	import datetime
	st = datetime.datetime.fromtimestamp(ts).strftime('%Y/%m/%d %H:%M:%S')
	#print(st,tag,message)
	output=open(logFilePath,"a")
	output.write(st+DELIM+tag+DELIM+message+DELIM+"\n")
	output.close()


def create_directory_if_not_there(dir,message):
	if not os.path.isdir(dir):
		os.mkdir(dir) # assume this can NOT fail???
		print(message,dir)


def save_dictionary_to_index_file(h_index):
	pickle.dump(h_index,open(indexFilePath,"wb"))


def init():
	""" creates a new archive directory, objects subdirectory, empty index file if they do not exist """
	#r=inspect.currentframe().f_code.co_name
	print(inspect.currentframe().f_code.co_name) # DRY
	#print((r)) 
	#print(inspect.getouterframes(inspect.currentframe()))
	#
	# display name of current routine, DRY do not repeat yourself
	create_directory_if_not_there(myArchiveDirPath,"Created archive directory : ")
	#
	save_dictionary_to_index_file({})
	#
	create_directory_if_not_there(objectsDirPath,"Created objects directory : ")
	#

def h_return_dictionary_of_index():
	""" called from store,list,test   - h_ prefix notation means is returning a dictionary (hash)"""
	#
	if os.path.isfile(indexFilePath):
		h_files=pickle.load(open(indexFilePath,"rb"))
		return h_files
	else:
		sys.exit("Index file does not exist - perhaps you need to run the init option? - exiting....")

def recursivelyListFilesAndGatherInList(dir,l_files):
	for relPath, l_dirs, l_filesInThisDir in os.walk(dir,topdown=False):
		for file in l_filesInThisDir:
			fullPath=relPath+os.sep+file
			l_files.append(fullPath)
			


# modified version of code provide as part of assignement
def createFileSignature(filename):
	"""CreateFileHash (file) returns the SHA1 hash of passed file"""
	BLOCK_SIZE=16384
	f=hashValue=None
	try:
		f = open(filename, "rb") # open for reading in binary mode
		s = f.read(BLOCK_SIZE)
		hash = hashlib.sha1()
		while (s):
			hash.update(s)
			s = f.read(BLOCK_SIZE)
			hashValue = hash.hexdigest()
		f.close()
	except IOError:
		hashValue = None
	except OSError:
		hashValue = None
	return(hashValue)

def copy_file_with_error_checking(tag,origin,destination):
	#if not os.path.isfile(origin):sys.exit("File not there - die "+origin)
	if os.path.isfile(origin):
		shutil.copy(origin,destination)		
		log(tag,"Copy "+origin+" to "+destination)
		print("Copying file "+origin+" to "+destination)
	else:
		print("Source file not there - "+origin)  # should send to stderr


def store():
	"""  backup a directory into the archive, backups all files/subdirs, can not backup just one file"""
	dir=""
	if len(sys.argv) > 2:
		dir=sys.argv[2] # pick up directory from the 2nd argument eg python myBackup store dir
	else:
		dir=input("Please input name of directory to archive : ")
	#
	print("dir : ",dir)
	if not os.path.isdir(dir):
		sys.exit("Exit - as directory "+dir+" is not there")
	#
	h_index=h_return_dictionary_of_index()
	#
	# if we make it this far, the directory is there, so log it
	log("STORE","Directory is "+dir)
	#
	l_files=[]  # list to store the paths of each file in this dir obtained by recursive list of all subdirs
	recursivelyListFilesAndGatherInList(dir,l_files)
	print("l_files",l_files)
	#
	# create a dictionary out of our list of files storing the file contents sha1 hash values
	h_files={file:createFileSignature(file) for file in l_files}  # notice snappy use of the list comprehension
	#
	# copy the files to the objects directory
	#
	countAlreadyThere=0	
	for [file,objectFilename] in h_files.items():
		destinationPath=os.path.join(objectsDirPath,objectFilename)
		if os.path.isfile(destinationPath):
			log("STORE","Object already there, just add index for file "+file)
			countAlreadyThere+=1
		else:
			copy_file_with_error_checking("STORE",file,destinationPath)
		# add this file to the index dictionary
		h_index[file]=objectFilename
	save_dictionary_to_index_file(h_index)  # not consistent in my naming, sometimes camel case, and where it counts, use underscores
	print("Count of files not added, because already in archive : ",countAlreadyThere)

def list():
	""" usage x list [pattern]
	    lists any path that contains the pattern
	    if no pattern, then displays paths of all files in the archive"""
	pattern=""
	if len(sys.argv) > 2:
		pattern=sys.argv[2]  # pick up pattern from 2nd argument if present eg python myBackup list myPattern
	#
	h_files=h_return_dictionary_of_index()
	for file in h_files.keys():
		print("file : ",file) # comment out?
		if pattern in file: # should we upper case these??? should we only search the filename and not the path?
			print("\tFound pattern in : ",file)

def output_line_to_stdout_and_log(testOutputLine1):
	print(testOutputLine1)
	log("TEST",testOutputLine1)


def test():
	"""
	checks integrity of the archive ie all index items are in archive dir, and all object files have correct hash
	"""	
	h_files=h_return_dictionary_of_index()
	log("TEST","")
	# look over index, look for non-present files, and noting filenames that do not match the file content SHA1 hash
	l_indexEntriesWithFilesMissing=[]
	l_filesWithNamesThatDoNotMatchTheirContentHash=[]
	for [file,objectFile] in h_files.items():
		if not os.path.isfile(os.path.join(objectsDirPath,objectFile)):
			l_indexEntriesWithFilesMissing.append([file,objectFile])
		elif objectFile != createFileSignature(os.path.join(objectsDirPath,objectFile)): 
			l_filesWithNamesThatDoNotMatchTheirContentHash.append(file)
	#
	# output phase
	output_line_to_stdout_and_log("Number of index entries : "+str(len(h_files.keys())))  # not asked for, but handy
	#
	numberCorrectIndexEntries=len(h_files.keys())-len(l_indexEntriesWithFilesMissing)-len(l_filesWithNamesThatDoNotMatchTheirContentHash)
	output_line_to_stdout_and_log("numberCorrectIndexEntries  : "+str(numberCorrectIndexEntries))
	#
	if len(l_indexEntriesWithFilesMissing) == 0:
		output_line_to_stdout_and_log("There are no erroneous paths")
	else:
		output_line_to_stdout_and_log("Names of the "+str(len(l_indexEntriesWithFilesMissing))+" erroneous path(s) that do not have matching files")
		[output_line_to_stdout_and_log("\t"+l1[0]) for l1 in l_indexEntriesWithFilesMissing]  # save 1 line using list comprehension
	#
	if len(l_filesWithNamesThatDoNotMatchTheirContentHash) == 0:
		output_line_to_stdout_and_log("All objects have a name that matches their hash")
	else:
		output_line_to_stdout_and_log("List of the "+str(len(l_filesWithNamesThatDoNotMatchTheirContentHash))+" object(s) that have a name that does NOT match its hash")
		for f in l_filesWithNamesThatDoNotMatchTheirContentHash:
			output_line_to_stdout_and_log("\t"+f+" "+h_files[f])



def get():
	"""
	usage mybackup get pattern
	lists any path that contains the pattern, if single match, then restore to current dir, if multiple then interactive prompt
	"""
	print("get")
	pattern=""
	if len(sys.argv) > 2:
		pattern=sys.argv[2]
	else:
		sys.exit("Usage: python "+sys.argv[0]+" get pattern") # NB: name of script not hardcoded
	h_files=h_return_dictionary_of_index()
	#
	# reading the assignment, it appears we are meant to be looking at the whole path, not just looking the filenames
	print("h_files : ",h_files)
	l_fileMatches=[]
	#for file in h_files.keys():  # maybe put this back in???
	#	base=os.path.basename(file)
	#	print("file : ",file," ",base)
	#	if pattern in base: # should we upper case these???
	#		print("\tfound pattern")
	#		l_fileMatches.append(file)
	[l_fileMatches.append(file) if pattern in os.path.basename(file) else None for file in h_files.keys()]
	#
	if len(l_fileMatches) == 0:
		print("No matches found")
	elif len(l_fileMatches) == 1:
		myBasename=os.path.basename(l_fileMatches[0])
		print("Pattern matches one file, thus restoring "+myBasename+" to current directory")
		orig=os.path.join(objectsDirPath,h_files[l_fileMatches[0]]) # error handling, check orig there?
		copy_file_with_error_checking("GET",orig,myBasename)
	else:
		# prompt
		#print("which ")
		# display menu of files in sorted order
		l_sortedFileMatches=sorted(l_fileMatches)
		for a in enumerate(l_sortedFileMatches[0:MAX_MATCHES],start=1):print(" ",a[0]," ",a[1]);
		indexString=input("Input number of file to restore : ")
		try:  # all this in the name of error handling,can handle non-integer and non-valid integers this way
			index=int(indexString)
			index-=1  # we use human friendly indexs starting at 1, so take off 1 to get correct element
			print("index : ",index,l_sortedFileMatches[index],h_files[l_sortedFileMatches[index]])
			#
			orig=os.path.join(objectsDirPath,h_files[l_sortedFileMatches[index]])
			dest=os.path.basename(l_sortedFileMatches[index])
			print("orig : ",orig," dest ",dest)
			if os.path.isfile(dest):
				overwrite=input("Overwrite (Y/N) : ")
				if overwrite.upper().startswith("Y"):
					copy_file_with_error_checking("GET",orig,dest)
		except:
			sys.exit("Invalid input for number of file : "+indexString)
		else:
			copy_file_with_error_checking("GET",orig,dest)


def restore():
	""" 
	Usage: mybackup restore [destinationDir]
	Restores everything in the archive to the destination directory
	If no destinationDir then uses the current directory
	"""
	destinationDirectory="."
	if len(sys.argv) > 2:
		destinationDirectory=sys.argv[2]
	#
	h_files=h_return_dictionary_of_index()
	#
	for [file,objectFile] in sorted(h_files.items()):  # ensure always does this in sorted order, handy while debugging
		#print("file : ",file,objectFile)
		source=os.path.join(objectsDirPath,objectFile)
		dest=os.path.join(destinationDirectory,file)
		l_path=os.path.split(dest)
		print("l_path : ",l_path)
		dest_path=l_path[0]
		print("dest_path :",dest_path)
		if not os.path.isdir(dest_path):
			try:
				os.makedirs(dest_path)

			except OSerror:
				print("Can not make "+dest_path)
		copy_file_with_error_checking("RESTORE",source,dest)


l_validOptions=("init","store","list","test","get","restore") # used to validate the first command line argument

#
# mainline
#
if (len(sys.argv) > 1) and (sys.argv[1] in l_validOptions):
	globals()[sys.argv[1]]()  # only works if not using modules
else:
	print("No or invalid arguments, valid args are : ",",".join(l_validOptions))
#
#