#!/usr/bin/env python3
# file: mybackup.py
# author: 93059239 Thomas Ekholm
# purpose: 159.251 Assignment 2
#
# note on prefix notation:
#  l_ = list
#  h_ = hash/dictionary
# all routines, when possible, declared before actual usage
#
import sys,os,os.path
import pickle,hashlib,time,datetime,shutil  # take performance hit by importing all modules even if not used in one particular run
import inspect # used to pick up name of the current routine to avoid hardcoding it.

# constants/globals
DELIM = "," # define the delimiter used in the logging
MAX_MATCHES=50 # define the maximum number of matches that will be displayed in routine get
#
myArchiveDirPath=os.path.expanduser("~"+os.sep+"myArchive")  # evaluated at run-time, so not really a constant
#
INDEX_FILENAME="index.txt"  
indexFilePath=os.path.join(myArchiveDirPath,INDEX_FILENAME)
#
OBJECTS_DIR="objects"
objectsDirPath=os.path.join(myArchiveDirPath,OBJECTS_DIR)
#
LOG_FILENAME="myBackupLog.txt" 
logFilePath=os.path.join(myArchiveDirPath,LOG_FILENAME)


def log(tag,message):
  """ logs new entries added to archive, and output from checking archive """
  ts = time.time()
  st = datetime.datetime.fromtimestamp(ts).strftime('%Y/%m/%d %H:%M:%S')
  #
  output=open(logFilePath,"a") # a=append
  output.write(st+DELIM+tag+DELIM+message.replace(DELIM,"")+DELIM+"\n") # since using printable delimiter, ensure logged message not contain it
  output.close()


def save_dictionary_to_index_file(h_index):  
  pickle.dump(h_index,open(indexFilePath,"wb"))  # assume once index file created, no need to exception-handle updating index


def init():
  """ creates a new archive directory, objects subdirectory, empty index file if they do not exist """

  def create_directory_if_not_there(dir,message):  # python can behave a little like a block structured language, nested procedure
    if not os.path.isdir(dir):
      try:
        os.mkdir(dir)
        print(message,dir)
      except:
        os.exit("Can not create directory : "+dir+" - die") 

  #
  create_directory_if_not_there(myArchiveDirPath,"Created archive directory : ")
  #
  save_dictionary_to_index_file({})  # save an empty dictionary
  #
  create_directory_if_not_there(objectsDirPath,"Created objects directory : ")
  #	

def h_return_dictionary_of_index():
  """ called from store,list,test etc  - h_ prefix notation means is returning a dictionary (h = hash)"""
  #
  if os.path.isfile(indexFilePath):
    return pickle.load(open(indexFilePath,"rb"))
  else:
    sys.exit("Index file does not exist - perhaps you need to run the init option? - exiting....")

def recursivelyListFilesAndGatherInList(dir,l_files):
  for relPath, l_dirs, l_filesInThisDir in os.walk(dir,topdown=False):
    for file in l_filesInThisDir:
      fullPath=relPath+os.sep+file
      l_files.append(fullPath)   


def createFileSignature(filename):
  """CreateFileHash (file) returns the SHA1 hash of passed file, based on code provided in assignment """
  BLOCK_SIZE=16384
  f=hashValue=None
  try:
    f = open(filename, "rb") # open for reading in binary mode
    s = f.read(BLOCK_SIZE)
    hash = hashlib.sha1()
    while (s):
      hash.update(s)
      s = f.read(BLOCK_SIZE)
      hashValue = hash.hexdigest()
    f.close()
  except IOError:
    hashValue = None
  except OSError:
    hashValue = None
  return(hashValue)


def copy_file_with_error_checking_prompt(tag,origin,destination,promptToOverwrite=False):
  if os.path.isfile(origin):
    if os.path.isfile(destination) and promptToOverwrite:
        overwrite=input("Overwrite (Y/N) : ")
        if not overwrite.upper().startswith("Y"): # yes, unstructured way of doing it...
          return
    shutil.copy(origin,destination)    
    log(tag,"Copy "+origin+" to "+destination)  # output log record
    print("Copying file "+origin+" to "+destination) # and also display message to standard output
  else:
    print("Source file not there - "+origin)  # possibly should send this message to stderr?


def store():
  """  backup a directory into the archive, backups all files/subdirs, can not backup just one file"""
  dir=""
  if len(sys.argv) > 2:
    dir=sys.argv[2] # pick up directory from the 2nd argument eg python myBackup store dir
  else:
    dir=input("Please input name of directory to archive : ")
  #
  if not os.path.isdir(dir):
    sys.exit("Exit - as directory "+dir+" is not there")
  #
  h_index=h_return_dictionary_of_index()
  #
  # if we make it this far, the directory is there, so log it
  thisRoutine=inspect.currentframe().f_code.co_name
  log(thisRoutine,"Directory is "+dir)
  #
  l_files=[]  # list to store the paths of each file in this dir obtained by recursive list of all subdirs
  recursivelyListFilesAndGatherInList(dir,l_files)
  #
  # create a dictionary out of our list of files storing the file contents sha1 hash values
  h_files={file:createFileSignature(file) for file in l_files}  # NB: snappy use of the list comprehension
  #
  # copy the files to the objects directory
  #
  countAlreadyThere=0  
  for [file,objectFilename] in h_files.items():
    destinationPath=os.path.join(objectsDirPath,objectFilename)
    if os.path.isfile(destinationPath):
      log(thisRoutine,"Object already there, just add index for file "+file)
      countAlreadyThere+=1
    else:
      copy_file_with_error_checking_prompt(thisRoutine,file,destinationPath)
    # add this file to the index dictionary, if already in index, not harmful
    h_index[file]=objectFilename
  save_dictionary_to_index_file(h_index)  # not consistent in my naming, sometimes camel case, and for extra clarity, use underscores
  print("Count of files not added, because already in archive : ",countAlreadyThere)


def list():
  """ usage x list [pattern]
      lists any path that contains the pattern
      if no pattern, then displays paths of all files in the archive"""
  pattern=""  # default pattern, which will pick up every file
  if len(sys.argv) > 2:
    pattern=sys.argv[2]  # pick up pattern from 2nd argument if present eg python mybackup list myPattern
  #
  # the following could be done as a 1 liner using list comprehension, done this way for clarity
  for file in sorted(h_return_dictionary_of_index().keys()):
    if pattern in file: # assignment says "any path that contains pattern", we make case-sensitive search
      print("\tFound pattern in : ",file)


def output_line_to_stdout_and_log(callingRoutine,testOutputLine1):
  print(testOutputLine1)
  log(callingRoutine,testOutputLine1)


def test():
  """ checks integrity of the archive ie all index items are in archive dir, and all object files have correct hash """  
  thisRoutine=inspect.currentframe().f_code.co_name
  #	
  h_files=h_return_dictionary_of_index()
  # look over index, look for non-present files, and noting filenames that do not match the file content SHA1 hash
  l_indexEntriesWithFilesMissing=[]
  l_filesWithNamesThatDoNotMatchTheirContentHash=[]
  for [file,objectFile] in h_files.items():
    if not os.path.isfile(os.path.join(objectsDirPath,objectFile)):
      l_indexEntriesWithFilesMissing.append([file,objectFile])
    elif objectFile != createFileSignature(os.path.join(objectsDirPath,objectFile)): 
      l_filesWithNamesThatDoNotMatchTheirContentHash.append(file)
  #
  # output phase
  output_line_to_stdout_and_log(thisRoutine,"Number of index entries : "+str(len(h_files.keys())))  # not asked for, but handy
  #
  numberCorrectIndexEntries=len(h_files.keys())-len(l_indexEntriesWithFilesMissing)-len(l_filesWithNamesThatDoNotMatchTheirContentHash)
  output_line_to_stdout_and_log(thisRoutine,"numberCorrectIndexEntries  : "+str(numberCorrectIndexEntries))
  #
  if len(l_indexEntriesWithFilesMissing) == 0:
    output_line_to_stdout_and_log(thisRoutine,"There are no erroneous paths")
  else:
    output_line_to_stdout_and_log(thisRoutine,"Names of the "+str(len(l_indexEntriesWithFilesMissing))+" erroneous path(s) that do not have matching files")
    [output_line_to_stdout_and_log(thisRoutine,"\t"+l1[0]) for l1 in l_indexEntriesWithFilesMissing]  # save 1 line using list comprehension
  #
  if len(l_filesWithNamesThatDoNotMatchTheirContentHash) == 0:
    output_line_to_stdout_and_log(thisRoutine,"All objects have a name that matches their hash")
  else:
    output_line_to_stdout_and_log(thisRoutine,"List of the "+str(len(l_filesWithNamesThatDoNotMatchTheirContentHash))+" object(s) that have a name that does NOT match its hash")
    for f in l_filesWithNamesThatDoNotMatchTheirContentHash:
      output_line_to_stdout_and_log(thisRoutine,"\t"+f+" "+h_files[f])


def get():
  """
  usage mybackup get pattern
  lists any path that contains the pattern, if single match, then restore to current dir, if multiple then interactive prompt
  """
  thisRoutine=inspect.currentframe().f_code.co_name
  pattern=""
  if len(sys.argv) > 2:
    pattern=sys.argv[2]
  else:
    sys.exit("Usage: python "+sys.argv[0]+" get pattern") # NB: name of script not hardcoded
  h_files=h_return_dictionary_of_index()
  #
  # reading the assignment, it appears we are meant to be looking at the whole path, not just looking at the filenames
  l_fileMatches=[]
  [l_fileMatches.append(file) if pattern in os.path.basename(file) else None for file in h_files.keys()] # build list of files that match pattern
  #
  if len(l_fileMatches) == 0:
    print("No matches found for pattern : ",pattern)
  elif len(l_fileMatches) == 1:
    myBasename=os.path.basename(l_fileMatches[0])
    print("Pattern matches one file, thus restoring "+myBasename+" to current directory")
    orig=os.path.join(objectsDirPath,h_files[l_fileMatches[0]])
    copy_file_with_error_checking_prompt(thisRoutine,orig,myBasename,promptToOverwrite=True)
  else:
    # display menu of files in sorted order
    l_sortedFileMatches=sorted(l_fileMatches)
    for a in enumerate(l_sortedFileMatches[0:MAX_MATCHES],start=1):print(" ",a[0]," ",a[1]);
    # prompt user to select file
    indexString=input("Input number of file to restore : ")
    try:  # all this in the name of error handling, can handle non-integer and non-valid integers this way
      index=int(indexString)
      index-=1  # we use human friendly index starting at 1, so take off 1 to get correct element
      #
      orig=os.path.join(objectsDirPath,h_files[l_sortedFileMatches[index]])
      dest=os.path.basename(l_sortedFileMatches[index])
      copy_file_with_error_checking_prompt(thisRoutine,orig,dest,promptToOverwrite=True)
    except:
      sys.exit("Invalid input for number of file : "+indexString)


def restore():
  """ 
  Usage: mybackup restore [destinationDir]
  Restores everything in the archive to the destination directory
  If no destinationDir then uses the current directory
  """
  destinationDirectory="."
  if len(sys.argv) > 2:
    destinationDirectory=sys.argv[2]
  #
  for [file,objectFile] in sorted(h_return_dictionary_of_index().items()):  # ensure always does this in sorted order, handy while debugging
    source=os.path.join(objectsDirPath,objectFile)
    dest=os.path.join(destinationDirectory,file)
    dest_path=os.path.split(dest)[0]
    if not os.path.isdir(dest_path):
      try:
        os.makedirs(dest_path)
      except:
        print("Can not make directory "+dest_path+" - it may already be there")
    copy_file_with_error_checking_prompt(inspect.currentframe().f_code.co_name,source,dest)  # 


l_validOptions=("init","store","list","test","get","restore") # used to validate the first command line argument

#
# mainline
#
if (len(sys.argv) > 1) and (sys.argv[1] in l_validOptions):
  globals()[sys.argv[1]]()  # only works if not using modules
else:
  print("No or invalid arguments, valid args are : ",",".join(l_validOptions))
#
#
