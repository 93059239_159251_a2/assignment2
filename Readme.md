#159.251 Software Construction Assignment 2#

##93059239 Thomas Ekholm##


##Running the Program##
Filename is mybackup.py.
At the windows command prompt,with python 3 installed, and on the path, and with the script in the current directory, use

*python mybackup.py*

If running you will get the following output:

*No or invalid arguments, valid args are :  init,store,list,test,get,restore*

If the script is in another directory, then use either relative or absolute path to that directory ie.

*python ..\\mybackup.py*

*python c:\\java_159_251\\python\\mybackup.py*

Under *nix, assuming python 3 is available as "python3", if script is in the current directory, use:

*python3 mybackup.py*

or similarly if script is in another directory use either relative of absolute paths ie.

*python3 ./scripts/mybackup.py*

*python3 /home/pi/scripts/mybackup.py*

Or make the script executable via

*chmod a+x mybackup.py*

Then the script can be run from the current directory via :

*./mybackup.py*

NB: This assumes that "env" is installed in /usr/bin.
If the current directory is included in the path (via PATH=$PATH:. or similar), then can run via:

*mybackup.py*

If the current directory is hardcoded into the path (via PATH=$PATH:/home/pi/scripts or similar), then can run from any directory via:

*mybackup.py*

The script has been tested to run, with NO changes, on:

 * Windows 8 (Microsoft Windows [Version 6.3.9600]) with Python version 3.4.3 (my laptop)
 * Raspberrian 3.12.22 with python 3.2.3 (my home raspberry pi)
 * Ubuntu 14.04.3 LTS (GNU/Linux 3.13.0-57-generic x86_64) with Python 3.4.0 (the Digital Ocean server)

Appendices B and C show the script running on the Linux platforms.


## git commits ##
Appendix A contains the full listing from "git log".

Significant commits are:

commitid : ab5f55e

commit ab5f55ed13148c54227879d7d86a429cfe529ae0

Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Mon Oct 12 21:42:24 2015 +1300

    restore() replace my hand-rolled code with os.makedirs() call. error handling in h_return_dictionary_of_index(), in case init has not been run. move above routine to be one of first calls in store(), list(), etc so force user to run init() have store() also use copy_file_with_error_checking() added code for store() to display names of new files, and count of those not added. fixed bug in get() introduced by having files display in sorted order

commitid : 174943d	

commit 174943d83f70d0f318b71fb04662f626676986ec

Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sun Oct 11 21:56:53 2015 +1300

    restore() now uses copy_file_with_error_checking(), change copy_file_with_error_checking() so if source file not there, just outputs errors, but does not die, which is required in restore(). ensure restore() always restores in sorted order. in restore(), replace for .keys() with .items() replace "\\" with os.sep, 303 lines of code


commitid : 2361f3b

commit 2361f3b6b9ed484912dd6a677b6952f55363fd94

Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Thu Oct 8 22:10:14 2015 +1300

    use sys.argv[0] in usage help screen, in route get() fixed bug in case of 1 pattern match was restoring the last file in the index, put in get() feature to display first 50 matches, came up with killer line to save 2 lines of code [l_fileMatches.append(file) if pattern in os.path.basename(file) else None for file in h_files.keys()] may regret it, 317 loc



##Features worth mentioning##
The script was written using notepad, not using PyCharm or any IDE.

The hash-bang syntax used is:
#!/usr/bin/env python3

I used a "hungarian" type prefix for variables and function return values of:
#  l_ = list
#  h_ = hash/dictionary
The philosophy here is : things should look like what they are.

All routines, when possible, are declared before actual usage.

I imported the modules at the top, thus taking a performance hit, but saving lines of code.

Rather than repeating function names, I have used inspect.currentframe().f_code.co_name) in the code, so functions can be renamed with no problems.


The major focus has been on code clarify rather than conciseness or run-time performance.  
So in most cases, but not all, I have resisted the temptation to use constructions like

  [l_fileMatches.append(file) if pattern in os.path.basename(file) else None for file in h_files.keys()] # build list of files that match pattern

instead of

for file in h_files.keys():

		base=os.path.basename(file)

		if pattern in base:

				l_fileMatches.append(file)


The final size of the code is 277 lines of code including comments and blank lines. One can find approximately the actual number of lines of code, excluding comments, via the following on Linux/git-bash environment:

*grep -v ^\\s*\\# mybackup.py | wc*

Excluding blank lines and comments mybackup.py is 176 lines of code.  The addition of exception handling adds quite a bit of extra code.

Look out in the code for the definition of "create_directory_if_not_there(dir,message)", I have nested it to show that python can behave a little like a block structured language.

I used the novel approach of storing valid command line options in a list, then calling the appropriate routine via:
  globals()[sys.argv[1]]() 
There is no object-orientated code in the script. 

In Appendix D there is the Test Suite used after each major change.



# Append A - output from "git log" #

commit 88c13fb464b347d08985944e9e4461ba251fb3e6
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Fri Oct 16 21:49:40 2015 +1300

    constants changed pure uppercase,in log(), ensure message text can not contain delimiter

commit bf3349b9f2a7a8ca8bf5ef9e9af618050ca2550e
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Thu Oct 15 22:10:39 2015 +1300

    implement prompt for get() overwrite in ALL cases by modifying copy_file_with_error_checking, removed some debugging, 290 lines of code

commit 5c4c403fc44a8326f868f05b7b1322078e995cb7
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Mon Oct 12 22:27:47 2015 +1300

    put error handling on interactive input in get(), tested code on raspberry pi running raspberrian 3.12.22 with python 3.2.3 - it ran fine with no changes.

commit ab5f55ed13148c54227879d7d86a429cfe529ae0
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Mon Oct 12 21:42:24 2015 +1300

    restore() replace my hand-rolled code with os.makedirs() call. error handling in h_return_dictionary_of_index(), in case init has not been run. move above routine to be one of first calls in store(), list(), etc so force user to run init() have store() also use copy_file_with_error_checking() added code for store() to display names of new files, and count of those not added. fixed bug in get() introduced by having files display in sorted order

commit 174943d83f70d0f318b71fb04662f626676986ec
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sun Oct 11 21:56:53 2015 +1300

    restore() now uses copy_file_with_error_checking(), change copy_file_with_error_checking() so if source file not there, just outputs errors, but does not die, which is required in restore(). ensure restore() always restores in sorted order. in restore(), replace for .keys() with .items() replace "\\" with os.sep, 303 lines of code

commit d27020dd53ce0148091ca5300614be51bbaa62cc
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Fri Oct 9 21:49:16 2015 +1300

    add start=1 parameter to enumerate, create routine copy_file_with_error_checking so error checking on copy in 1 place, in get() same command returned different sort order, so put in explicit sort to guarantee same result for same command

commit 2361f3b6b9ed484912dd6a677b6952f55363fd94
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Thu Oct 8 22:10:14 2015 +1300

    use sys.argv[0] in usage help screen, in route get() fixed bug in case of 1 pattern match was restoring the last file in the index, put in get() feature to display first 50 matches, came up with killer line to save 2 lines of code [l_fileMatches.append(file) if pattern in os.path.basename(file) else None for file in h_files.keys()] may regret it, 317 loc

commit af004f8727ddc36dda862b22c814d3c97bb40f69
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Wed Oct 7 21:49:38 2015 +1300

    in routine test() change if else if to if elif construct, create new routine to output to both stdout and log, use this many times, add count of index entries - even though not asked for, now 322 lines of code

commit 27c8cca8cdd1bd747b97a178d0afe00fb9b43abb
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Tue Oct 6 22:14:35 2015 +1300

    replace dictname.keys() with dictname.items in routines init and test for clarity, 341 lines of code

commit 32243f40daf0b1bf97903d0a4b386f8f276cabcc
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Tue Oct 6 21:37:19 2015 +1300

    subroutinised duplicate code in init routine, down to 344 lines of code including comments

commit 2f2459fd47f1387fce4096a33ba15496468fd55c
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Mon Oct 5 22:25:34 2015 +1300

    put const in block size in sha routine, simplified code, removed some unused test code, resolved bug in init, was not reseeting picked dictionary

commit 54865d724b69c85045558caad3ce9e014ebceeab
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sat Oct 3 22:07:39 2015 +1300

    move a few import statements to top of file, use os.sep instead of "\\", used createFileSignature in list comprehension creating dict from list., created function to save index dict to index file

commit 850271da2ea33b6c46c6ee4510a3debc1bf2a42d
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sat Oct 3 18:31:49 2015 +1300

    in routine init, removed code to del existing object files, improved screen feedback, simplified file/dir existence tests, added code to display current routine, 400 lines of code

commit 2cc92867e7a8edba5c4cde2c62c04689f17b2f3b
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sat Oct 3 17:48:53 2015 +1300

    first cut of logging, 419 lines at this stage

commit cf2e3e9420503f85450c2958cb819f1cad0c7c95
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sat Oct 3 14:10:39 2015 +1300

    restore option appears to be working now, no thorough testing yet, code rough and unmodularised

commit 48278bc798f295fc82b5a9157591b7ef2164e7fb
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Thu Oct 1 22:06:00 2015 +1300

    first cut on restore option, crashes on file copies

commit 1b7db5bd458134c8f41355d43ab9e4fd7734a7ef
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Thu Oct 1 21:40:46 2015 +1300

    first cut on get option, rough code but functional, needs more subroutinisation, list module now using the function to get index hash

commit 25112b0ee987b4ab4dcbb9c2b4c5e729286f6ac1
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Wed Sep 30 21:46:44 2015 +1300

    first cut on test option, no emphasis on output formatting or code structure

commit 29173bdc60142bc0a1bcb428d6326f5ca793b0fc
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Tue Sep 29 22:03:39 2015 +1300

    add comments on naming conventions, make reading index a routine, first cut of test routine

commit 3d9f9035cd7d444f7b51ac0af80e77a97a1074c1
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Tue Sep 29 21:27:42 2015 +1300

    Fixes to init, first cut of the store routine

commit fa4e4cbbb231dbaded6349e29d30fa3bb4f3e00a
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sun Sep 27 18:48:15 2015 +1300

    Command line processing code, and first cut of init routine

commit a64353955d59f6b2b06301255f23a036bb34e047
Author: 93059239 Thomas Ekholm <wenix12@yahoo.com>
Date:   Sun Sep 27 15:31:26 2015 +1300

    Initial commit with contributors


## Appendix B - script running on Ubuntu ##

![Script running under Ubuntu Linux](digital_ocean_shot.JPG)


## Appendix C - script running on Raspberrian ##

![Script running under Raspberrian](raspberry_shot.JPG)

















































## Appendix D - Test Suite - with sample output##

Do NOT run init initially, and run all other options to see if handled eg

python mybackup.py store

python mybackup.py list

python mybackup.py test

python mybackup.py get f

python mybackup.py restore


python mybackup.py

No or invalid arguments, valid args are :  init,store,list,test,get,restore


python mybackup.py init

Created archive directory :  C:\\Users\\User\\myArchive

Created objects directory :  C:\\Users\\User\\myArchive\\objects


python mybackup.py list


python mybackup.py store sub

Copying file sub\\walk.py to C:\\Users\\User\\myArchive\\objects\\1c935ef76eb1fa86bae760718b47fa2fe7ee3e1

Copying file sub\\sub1\\file1.txt to C:\\Users\\User\\myArchive\\objects\\3e55f1bde7e040d01a26f69a82ecbaf9ea0b3ef

Copying file sub\\out1.txt to C:\\Users\\User\\myArchive\\objects\\1fd9fb2e868ba56ba32946bfbce54363f5a066a

Copying file sub\\sub1\\file2.txt to C:\\Users\\User\\myArchive\\objects\\0af9c6896ec825816a49755c00b345fd2dc05af

Copying file sub\\sub1\\sub2\\file3.txt to C:\\Users\\User\\myArchive\\objects\\60fb915fe3a820909474b0507115a5effa320bf

Count of files not added, because already in archive :  0


python mybackup.py store sub

Count of files not added, because already in archive :  5


python mybackup.py store dirThatdoesNotExist

Exit - as directory dirThatdoesNotExist is not there


python mybackup.py store sub

python mybackup.py store sub -

NB: not harmful, just get extra log entries saying "Object already there, just add index for file sub\\out1.txt,". 
Not worth writing more code to handle this harmless edge-case


python mybackup.py list

        Found pattern in :  sub\\out1.txt

        Found pattern in :  sub\\sub1\\file1.txt

        Found pattern in :  sub\\sub1\\file2.txt

        Found pattern in :  sub\\sub1\\sub2\\file3.txt

        Found pattern in :  sub\\walk.py


python mybackup.py get

Usage: python mybackup.py get pattern


python mybackup.py get nomatch

No matches found


python mybackup.py get py

Pattern matches one file, thus restoring walk.py to current directory

Overwrite (Y/N) : y

Copying file C:\\Users\\User\\myArchive\\objects\\1c935ef76eb1fa86bae7e60718b47fa2fe7ee3e1 to walk.py


python mybackup.py get file

  1   sub\\sub1\\file1.txt

  2   sub\\sub1\\file2.txt

  3   sub\\sub1\\sub2\\file3.txt

Input number of file to restore : 3

Overwrite (Y/N) : y

Copying file C:\\Users\\User\\myArchive\\objects\\60fb915fce3a820909474b0507115a5effa320bf to file3.txt


python mybackup.py restore myrestore10

Copying file C:\\Users\\User\\myArchive\\objects\\1fd9fb2e868ba56ba325946bfbce54363f5a066a to myrestore10\\sub\\out1.txt

Copying file C:\\Users\\User\\myArchive\\objects\\3e55f1bde7e0430d01a26f69a82ecbaf9ea0b3ef to myrestore10\\sub\\sub1\\file1.txt

Copying file C:\\Users\\User\\myArchive\\objects\\0af9c6896ec82d5816a49755c00b345fd2dc05af to myrestore10\\sub\\sub1\\file2.txt

Copying file C:\\Users\\User\\myArchive\\objects\\60fb915fce3a820909474b0507115a5effa320bf to myrestore10\\sub\\sub1\\sub2\\file3.txt

Copying file C:\\Users\\User\\myArchive\\objects\\1c935ef76eb1fa86bae7e60718b47fa2fe7ee3e1 to myrestore10\\sub\\walk.py

